﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class High : MonoBehaviour
    {
    public static int score;
    int highScore;

    Text text;


    void Awake()
    {
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        print("HighScore: " + highScore);

        text = GetComponent<Text> ();

        score = 0;
    }


    void Update()
    {
        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("HighScore", highScore);
        }
        text.text = "HighScore: " + highScore;
    }
}
