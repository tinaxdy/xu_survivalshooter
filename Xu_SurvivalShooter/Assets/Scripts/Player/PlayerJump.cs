﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour {

    public float speed = 6;

    public Rigidbody rb; 

    public LayerMask groudLayers;

    public float jumpForce = 7;

    public CapsuleCollider col;

    public Transform GroundCheck;

    public bool OnGround;



    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();

    }

    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
        rb.AddForce(movement * speed);

        OnGround = IsGrounded();

        if(Input.GetKeyDown(KeyCode.Space)   &&    OnGround){
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }


    private bool IsGrounded()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(GroundCheck.transform.position, Vector3.down, out hitInfo, .03f))
        {
            return true;
        }
        else
            return false;

    }

}
